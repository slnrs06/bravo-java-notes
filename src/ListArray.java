import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListArray {
// converting arraylist to an array


    public static void main(String[] args) {

        List<String> list = new ArrayList<String>();
        list.add("a");// adding to arraylist called list
        list.add("b");
        String [] array = new String[list.size()];  // declaring String array defining new array taking in arraylist size
        list.toArray(array); // take arraylist called list make it an array take in array
        System.out.println("arraylist to array:" + Arrays.toString(array));

    }



}
