package Abstract;

public class Pet extends Animal {

    public void change (){
        System.out.println(" i am a pet and a cat");
    }

    @Override
    public void eat() {
             System.out.println("cat and pet eating.");
    }
}
