package Abstract;

public class Driver {
    public static void main(String[] args) {
        Cat cat1 = new Cat();
        Animal cat2 = new Cat();
        Pet cat3 = new Pet();
        cat3.eat();
    }
}
