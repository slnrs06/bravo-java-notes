abstract class Mammal {
    // abstract classes cannot be instantiated the below code will return an error

//    Mammal m1 = new Mammal();

    public void displayInfo(){
        System.out.println("i am a mammal. ");
    }



}

class Dolphin extends Mammal{

    void Mammal(){

    }
}
class Main {
    public static void main(String[] args) {
        Dolphin d1 = new Dolphin();
        d1.displayInfo();
    }
}
//
////abstract class Dolphin extends Mammal {  // inherited sub class
//class Dolphin extends Mammal
//
//
//}
