import java.util.Scanner;

public class ConsoleExercise {

    public static void main(String[] args) {

//        double pi = 3.14159;
//        System.out.println("The value of pi is approximately " + pi);

//        System.out.println("The value of pi is approximately 3.14");

//        System.out.format("The value of pi is approximately, %.2f\n", pi);   --correct answer  -- \n means new line  f is for float format

        Scanner scanner = new Scanner(System.in);

//        System.out.print("Enter an integer: ");

//            int userInput = scanner.nextInt()
//        int userInput = scanner.next();

//        System.out.println(userInput);


// stephen code
//        System.out.print("Enter three words: ");
//        String word1 = scanner.next();
//        String word2 = scanner.next();
//        String word3 = scanner.next();
//
//        System.out.println(word1 + "\n" + word2 + "\n" + word3);
//        System.out.println(word2);
//        System.out.println(word3);


//        System.out.println("Enter a sentence");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);

//        System.out.println("what is the length and width of the classroom");
//
        System.out.println("What is the length and width of the classroom? ");


        String length = scanner.nextLine();
//            int l = Integer.parseInt(length);
        Double l = Double.parseDouble(length);  // decimal
//            float l = Float.parseFloat(length);

        String width = scanner.nextLine();
//            int w = Integer.parseInt(width);
        Double w = Double.parseDouble(width);  // decimal
//        float w = Float.parseFloat(width);

        System.out.println("the perimeter of the classroom is " +(2 * w + 2 * l));
        System.out.println("the area of the classroom is " + (w * l));
//



//        GOOGLE RESOURCE:

//        System.out.print("L:");
//        Scanner s = new Scanner(System.in);
//        try{
//            // convert the string read from the scanner into Integer type
//            Integer length = Integer.parseInt(s.nextLine());

//            System.out.print("W:");
//            s = new Scanner(System.in);
//            Integer width = Integer.parseInt(s.nextLine());
//            // Printing the area of rectangle
//            System.out.println("Area of classroom:"+width*length);
//        }
//        catch(NumberFormatException ne){
//            System.out.println("Invalid Input");
//        }
//        finally{
//            s.close();
//        }
//

//        do you capture all of the words?

//        Rewrite the above example using the .nextLine method.
//        String userInput = scanner.nextLine();
//        Calculate the perimeter and area of Code Bound's classrooms

//        Use the .nextLine method to get user input and cast the resulting string to a numeric type.

//•	Assume that the rooms are perfect rectangles.
//•	Assume that the user will enter valid numeric data for length and width.


    }









} //END OF CLASS

