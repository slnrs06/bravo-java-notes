interface Inter { // interface 1

    public void display1(); // abstract method to be implemented on object creation// cannot have body-- only take abstract methods

}
// second interface

interface Example2{ // example 2 has an abstract method of display 2
    public void display2();
}
// the above code contains 2 interfaces with 2 separate abstract methods



// this interface is extending both the above interface

interface Example3 extends Inter, Example2{ // this interface is using INHERITANCE to extend Interface Example

}

// IMPLEMENTING INTERFACE CALLED EXAMPLE 3 USING THE KEYWORD IMPLEMENT

class Example4 implements Example3 { // this class is implementing Example 3 because Example 3 inherits interface then it mus implement interface methods

   @Override
    public void display1(){  // using method overriding here to OVERRIDE display1 method in interface
        System.out.println("Display2 method");
    }

    @Override
    public void display2() {  // using method OVERRIDING here to override display2 method in interface
        System.out.println("Display 3 method");
    }
}

class Demo{  // just a simple class to run our program
    public static void main(String[] args) {
        Example4 obj = new Example4();
        obj.display1();
    }
}



