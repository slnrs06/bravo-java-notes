package OOP;

/** OBJECT - AN INSTANCE OF A CLASS
 - has properties and methods
  - Instantiated with the new keyword
  - Class - template or blueprint the we use for objects
*/






// creating class
public class BankAccount {

// creating instance variable
private double balance;

    // creating static variable //
    public static double interestRate;

    // creating a static method//

        public static double getInterestRate(){    ////getter
            return interestRate;
        }

        public static double setInterestRate(double ir){  // setter
            interestRate = ir;
            return ir;
        }



//getter
public double getBalance(){
    return balance;
}
// setting
 public void setBalance(double balance){
    this.balance = balance;


 }







}// end of class
