import java.util.Scanner;

public class HelloWorld {
    // psvm gives main method

    public static <Oject> void main(String[] args) {
        // sout
//        System.out.println("Hello, World!");
//
//        System.out.println("code inside of the curly braces");

//   DATA TYPES

    //  8 PRIMITIVE DATA TYPES    data is byte  age is variable
        byte age = 12;
        short myShort = -32768;
        int myInt = 120;
        long myLong = 1234567894359L;
        float myFloat = 123.342F;
        double myDouble = 123.34;
        char myChar = 'B';
        boolean myBoolean = true;

//        int x = 10;
//        int y = 25;

//        System.out.println(x+y);
//        System.out.println("X times Y = " + (x * y));

//        byte myByte2 = -127;    ---MAX VALUE

//          DATATYPE AND VARIABLE EXERCISE
//        Create an int variable named favoriteNum and assign your favorite number to it,
//        then print it out to the console.

//            int favoriteNum = 7;
//        System.out.println(favoriteNum);

//        Create a String variable named myName and assign your first name as a string
//        value to it, then print the variable out to the console.
//            String myName = "Sandra";
//        System.out.println(myName);

//        Change your code to assign a character value to myName. What do you notice?
//        char myInitial = 'S';
//        System.out.println(myInitial);

//        Change your code to assign the value 3.14159 to myName. What happens?
//        double myDName = 3.14159;
//        System.out.println(myDName);


//        Declare an long variable named myNum, but do not assign anything to it.
//        Next try to print out myNum to the console. What happens?
//        long myNum = 314L;
////        myNum = 3.14159L;
//        System.out.println(myNum);

//        Change your code to assign the value 3.14 to myNum. What do you notice?

//        Change your code to assign the value 123L (Note the 'L' at the end) to myNum.

//        Change your code to assign the value 123 to myNum.

//        Why does assigning the value 3.14 to a variable declared as a long not compile,
//        but assigning an integer value does?

//        Change your code to declare myNum as a float. Assign the value 3.14 to it.
//        What happens? What are two ways we could fix this?

//        float mNum = 3.14f;

//        Copy and paste the following code blocks one at a time and execute them
//        int x = 10;
//        int x = 10;
//        System.out.println(x++);
//        System.out.println(x);
//        System.out.println(++x);

//        System.out.println(x++);

//        System.out.println(x);

//        int x = 10;

//        System.out.println(++x);

//        System.out.println(x);

//        What is the difference between the above code blocks? Explain why the code
//        outputs what it does.

//        Try to create a variable named class. What happens?

//        Object is the most generic type in Java. You can assign any value to a variable of
//        type Object. What do you think will happen when the following code is run?

//        String theNumberEight = “eight”;
//            String theNumberEight = "eight";
//            Object o = theNumberEight;
//            int eight = (int)"eight";
//        System.out.println();

//        Object o = theNumberEight;

//        int eight = (int) o;

//        Copy and paste the code above and then run it. Does the result match with your expectation?

//        How is the above example different from this code block?
//        int eight = (int) “eight”;

//        What are the two different types of errors we are observing?

//        Rewrite the following expressions using the relevant shorthand assignment operators:
//        int x = 5;
//        x = x + 6;
//        int x = 7;
//        int y = 8;
//        y = y * x;
//        int x = 20;
//        int y = 2;
//        x = x / y;
//        y = y - x;
//        What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold? What happens if you increment a numeric variable past the type's capacity?
//        Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type. (edited)

//            int x = 5;
//            x = x +6;
//                x+= 6;
//        System.out.println(); // 11

//            int x = 7;
//            int y = 8;
//             y = y * x;
//             y *= x;

//                int x = 20;
//                int y =2;
////                x = x / y;
//                    x /= y;

//                shorthand assignment operator
//                    ++
//                    --
//                    +=
//                    -=
//                    *=
//                    /=


//                    int z = Integer.MAX_VALUE +1;
//                       System.out.println(z);


//              PRINTING OUR OUTPUT
//                    String name = "Bravo";
//        System.out.format("Hello there, %s. Nice to see you.", name);

//        String greet = "Hola";
//        System.out.format("%s, %s!", greet, name);


        //          SCANNER CLASS -GET INPUT FROM THE CONSOLE
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter something: ");  // prompt the user to enter data
//        String userInput = scanner.next();   // obtaining the value the user input
//        System.out.println(userInput);  // souting the userInput






                }  // END of main method


} // END of class
