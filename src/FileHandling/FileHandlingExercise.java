package FileHandling;

import java.io.*;

public class FileHandlingExercise {

    public static void main(String[] args) {
        String line;
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("./albums/DarkSideOfTheMoon.txt"));
//            bufferedReader = new BufferedReader(new FileReader("./albums/MaroonV.txt"));
            line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        }   catch (IOException e) {
            System.out.println(e.getMessage());
        }   finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }



            String text =
                    "Album: Darkside of the Moon\n" +
                            "======== \n" +
                            "Artist: Pink Floyd\n" +
                            "Year: 1978\n" +
                            "Genre: Rock" +
                            "======== ";

            String text2 =
                    "Album: MaroonV\n" +
                            "======== \n" +
                            "Artist: MaroonFive\n" +
                            "Year: 2014\n" +
                            "Genre: Pop Rock" +
                            "======== ";

            try (BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/DarkSideOfTheMoon.txt",true)))
            {
                writer.write(text);
                writer.newLine();
            }

//            try (BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/MaroonV.txt",true)))
//            {
//                writer.write(text2);
//                writer.newLine();
//            }


            catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }










    }// end of main method







}// end of class
