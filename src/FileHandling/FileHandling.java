package FileHandling;

import java.io.*;
import java.nio.file.FileVisitResult;

/*
File, BufferedReader, FileReader, BufferedWriter, and FileWriter from java.io package.
how to import?
import java.io.*;
 */
public class FileHandling {
    public static void main(String[] args) {

   // READING IF THERE'S A TEXT FILE WITH A NAME... use the FileReader class
   /*
   FileReader read the content of a file
   - needs to be WRAPPED by a BufferedReader object
    */
      // create a String variable
//      String line;
//
//      // create a BufferedReader object
//        BufferedReader bufferedReader = null;
//
//        // try-catch-finally statement
//        try{
//            bufferedReader = new BufferedReader(new FileReader("./movieQuotes/hamlet.txt"));
//
//            line = bufferedReader.readLine();// read whats inside txt file
//
//            while (line != null){
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }
//
//        }catch (IOException e){
//            System.out.println(e.getMessage());
//        }
//        finally {
//            try{
//                if (bufferedReader != null){
//                    bufferedReader.close();
//                }
//            }
//             catch (IOException e){
//                 System.out.println(e.getMessage());
//             }
//
//        }

        // WRITING TO A TEXT FILE...

        // create a string variable
//        String text = "I'll be back.";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/terminator.txt", true)))
//        {
//            writer.write(text);
//            writer.newLine();
//        }
//        catch (IOException e){
//            System.out.println(e.getMessage());
////            System.out.println("custom error message");
//        }
//
//        // OVERWRITING A TEXT FILE / FILES'S TEXT ....?
//
//        // CREATING A STRING VARIABLE
//        String overwriteText = "Listen to many, speak to a few.";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/hamlet.txt")))
//        {
//            writer.write(overwriteText);
//            writer.newLine();
//
//        }
//        catch (IOException e){
//            System.out.println(e.getMessage());
//        }

//  RENAMING A TEXT FILE...
//        File oldFileName = new File("./movieQuotes/hamlet.txt"); // targeting the file we want to rename.
//        File newFileName = new File("./moveQuotes/shakespeareHamlet.txt");// creating name we want
//
//        oldFileName.renameTo(newFileName);


// DELETING A TEXT FILE ...
//        File newFileName = new File("./moveQuotes/shakespeareHamlet.txt");// creating name we want
//       newFileName.delete(); // cli 'rm-r filename

//        String line;
//        BufferedReader bufferedReader = null;
//        try {
//            bufferedReader = new BufferedReader(new FileReader("./albums/DarkSideOfTheMoon.txt"));
//            line = bufferedReader.readLine();
//            while (line != null) {
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }
//        }   catch (IOException e) {
//            System.out.println(e.getMessage());
//        }   finally {
//            try {
//                if (bufferedReader != null) {
//                    bufferedReader.close();
//                }
//            } catch (IOException e) {
//                System.out.println(e.getMessage());
//            }
//
//
//            String text =
//                    "Album: Darkside of the Moon\n" +
//                            "======== \n" +
//                    "Artist: Pink Floyd\n" +
//                            "Year: 1978\n" +
//                            "Genre: Rock" +
//                            "======== ";
//            try (BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/DarkSideOfTheMoon.txt")))
//            {
//                writer.write(text);
//                writer.newLine();
//            } catch (IOException e) {
//                System.out.println(e.getMessage());
//            }
//        }
//

//
//        BufferedReader bufferedReader = null;
//
//        try {
//            bufferedReader = new BufferedReader(new FileReader("./albums/maroonV.txt"));
//            line = bufferedReader.readLine();
//            while (line != null){
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }
//        }
//        catch(IOException e){
//            System.out.println(e.getMessage());
//        }
//        finally {
//            try {
//                if (bufferedReader != null){
//                    bufferedReader.close();
//                }
//            }
//            catch (IOException e){
//                System.out.println(e.getMessage());
//            }
//        }
//
//        String text =
//        "Album: maroonV\n" +
//                "======== \n" +
//                "Artist: MaroonFive\n" +
//                "Year: 2014\n" +
//                "Genre: Pop Rock" +
//                "======== ";
//
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/maroonV.txt")))
//        {
//
//        }


    }// end of main method





}// end of class
