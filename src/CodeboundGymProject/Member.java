package CodeboundGymProject;

public class Member {
//public String getMemberName()



    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    private String memberName;


    public double getMemberFees() {
        return memberFees;
    }

    public void setMemberFees(double memberFees) {
        this.memberFees = memberFees;
    }


    private double memberFees;



    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    private String memberType;


    public double getMemberId() {
        return memberId;
    }

    public void setMemberId(double memberId) {
        this.memberId = memberId;
    }

    private double memberId;




    public String toString() {
        return "Member{" +
                "memberType='" + memberType + '\'' +
                ", memberId=" + memberId +
                ", name='" + memberName + '\'' +
                ", fees=" + memberFees +
                '}';
    }

    public Member(String name, double fees, String memberType, double memberId) {


    memberName =name;
    memberFees =fees;
    this.memberType =memberType;
    this.memberId =memberId;

}






}// end of class
