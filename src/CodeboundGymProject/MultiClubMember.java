package CodeboundGymProject;

public class MultiClubMember extends Member {

    public int getMembershipPoints() {
        return membershipPoints;
    }

    public void setMembershipPoints(int membershipPoints) {
        this.membershipPoints = membershipPoints;
    }

    private int membershipPoints;

    public MultiClubMember(String name, String memberType, double fees, double memberId) {
        super(memberType, memberId, name, fees);
    }

    @Override
    public String toString() {
        return "MultiClubMember{" +"memberType='" + getMemberType() + '\'' +
                ", memberId=" + getMemberId() +
                ", name='" + getMemberName() + '\'' +
                ", fees=" + getMemberFees() + '\'' +
                "membershipPoints=" + membershipPoints +
                '}';
    }

    public static void main(String[] args) {

    }// end of
}// end of class
