import java.util.Arrays;

public class StringMethodsPractice {
    public static void main(String[] args) {

//        int myLength = "Good afternoon, good evening, and good night"
//        Write some java code that will display the length of the string
//
//        String myLength = "Good afternoon, good evening, and good night";
//
//        System.out.println(myLength.length());
//
////
//////        Write some java code that will display the entire string in all uppercase
//////
//    System.out.println(myLength.toUpperCase());
////
//////        Use the same code but instead of using the toUpperCase() method, use the
//////    toLowerCase() method.
//        System.out.println(myLength.toLowerCase());
////
//
////        Copy this code into your main method
//        String firstSubstring = "Hello World".substring(6);
//        System.out.println(firstSubstring);
//
////        Change the argument to 3, print out in the console.
////        What do you get?
//        String firstSubstring = "Hello World".substring(3);
//        System.out.println(firstSubstring);
//
//        String firstSubstring = "Hello World".substring(10);
//        System.out.println(firstSubstring);


//        Copy this code into your main method
//        Using the substring() method, make your variable print out "Good evening"

//        String message = "Good evening, how are you?";
//        System.out.println(message.substring(0,12));

//        Now using the substring() method, try to make your variable
//        print out "how are you?"

//        String message = "Good evening, how are you?";
//        System.out.println(message.substring(14,26));
//
//
////        Create a char variable named 'myChar' and assign the value "San Antonio"
////
//        String myChar = "San Antonio";
//        System.out.println(myChar);

//        Using the charAt() method return the capital 'S' only.
//        System.out.println(myChar.charAt(0));
//
////        Change the argument in the charAt() method to where it
////        returns the capital 'A' only.
//
//        System.out.println(myChar.charAt(4));
//
////        Change the argument in the charAt() method to where it returns
////        the FIRST lowercase 'o' only.
//        System.out.println(myChar.charAt(7));


//        Create a String variable name 'bravo' and assign all the names
//        in the cohort in ONE string.

//            String bravo = "MaryAnn, Jonathan, Adrian, Eric, Sandra, Henry";
//        System.out.println(bravo);


//        Using the split() method, create another variable named 'splitBravo' where
//        it displays the list of names separated by a comma.
        /*
        String bravo = "MaryAnn Jonathan Adrian Eric Sandra Henry";
        String [] splitBravo = bravo.split(",")
        System.out.println(Arrays.toString(splitBravo));
         */

//            String splitBravo = "MaryAnn, Jonathan, Adrian, Eric, Sandra, Henry";
//        System.out.println(Arrays.toString(bravo.split(",")));



//        Use the following string variables.
//         String m1 = "Hello, ";
//        String m2 = "how are you?";
//        String m3 = "I love Java!";
//
//        System.out.println(m1 + m2 + " " + m3);
//
//
////        Use the following integer variable
//        int result = 89;
//
//        System.out.println("you scored" + result + "marks for your test");

       //  STRING DRILLS
//
//        String message = "I never wanna hear you say\nI want it that way";
//        String message = "Check \"this\" out!, \"s inside of \"s!\"";
//        System.out.println(message);

//        String message = "In windows, the main drive is usually C:\\\n"+
//        "I can do backslashes \\, double backslashes \\\\,\n"+
//        "and the amazing triple backslash \\\\\\!";
//        System.out.println(message);









    }


} // END OF CLASS
