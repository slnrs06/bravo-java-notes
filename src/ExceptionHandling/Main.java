package ExceptionHandling;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

//        int age;
//        System.out.println("Welcome to the club!");
//        System.out.println("Please enter your age: ");
//
//        try {
//            // what we want to do here ...
//            age = userInput.nextInt();
//
//            if (age < 0 || age > 125){
//                System.out.println("Invalid age");
//                System.out.println("Age must be between 0 and 124");
//            }else if (age < 18){
//                System.out.println("Sorry you are under age");
//            }else if (age > 21){
//                System.out.println("Welcome in");
//            }
//
//        }
//         catch (InputMismatchException e){
//             System.out.println("I said to enter your age loser...");
//         }
//         finally {
//            System.out.println("This block will always run, no matter what");
//        }


        // EXAMPLE TWO
        int numerator, denominator;
//        int numerator,
//                int denominator;

        try{
            // what we want to do...
            System.out.println("Enter the numerator: ");
            numerator = userInput.nextInt();

            System.out.println("Enter denominator:");
            denominator = userInput.nextInt();
            System.out.println("the result is: " + numerator/denominator);
        }
//            catch (Exception e){
//                System.out.println(e.getMessage());
//            }

        catch (InputMismatchException e1){
            System.out.println(e1.getMessage());
        }
        finally {
            System.out.println(" - - - End of Error Handling Example - - -");
        }

    }// end of main method



}// end of class
