package JavaIIAbs;
// using abstract class inherit (extends) one abstract class at at time or can extend
public class Example1 {

public void display1(){
    System.out.println("display one method");
}

}

abstract class Example2 {
    public void display2(){
        System.out.println("display 2 method");
    }
}

abstract class Example3 extends Example1{
    abstract void display3();
}

class Example4 extends Example3{

//    @Override
    void display3() {
        System.out.println("i am on display....");
    }
}

class Demo{

    public static void main(String[] args) {
        Example4 obj = new Example4();
        obj.display3();

    }

}
