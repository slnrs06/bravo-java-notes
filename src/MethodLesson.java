public class MethodLesson {
    public static void main(String[] args) {
//        METHODS - ARE A SEQUENCE OF STATEMENTS THAT PERFORM A SPECIFIC TASK.
//        CALL MY greeting()
//        (greeting("Hello", "bravo");
//        System.out.println(returnFive());

//        System.out.println(yelling("hello world"));


//            String changeme = "Hello Bravo!";
//            changeString(changeme);
//        System.out.println(changeme);

        counting(5);



    } // END OF MAIN METHOD

//     basic syntax for a method:

//    public static returnType methodName(param1, param2,){
////        we want the code to do.
//    }

//    public static String greeting(String name){
//        return String.format("Hello %s!", name); // %placeholder for a string
//    }
//        public static void greeting(String greet, String name);{
//        System.out.println("%s, %s!\n" greet, name);
//    }
//
//    public static int returnFive(){
//        return 5;
//    }
//
//    public static String yelling (String s){
//        return s.toUpperCase()
//    }


//      public - this is the visibility modifier

//          defines whether or not other classes can "see" this method.
//      static - the presence of this keyword defines that the method
//          belongs to the class, as opposed to instances of it.


        //  METHOD OVERLOADING
//     - defining multiple methods with the same name.
//      but with different parameter type, parameter order,
//      or number of parameters

        public static String message () {
            return "This is an example of methods";
        }

        public static String message(String memo){
            return memo;
        }

        public static String message(int code){
            return "Code: " + code + " message";
        }

        public static String message(String memo, String status){
            return memo + "\nStatus: " + status;
        }


//            PASSING PARAMETERS TO METHODS
//        public static void changeString(String s){
//            s = "This is a string";
//        }

//          RECURSION -  that aims to solve a problem by dividing it
//           into small chunks
//          counting 5 to 1 using recursion
            public static void counting(int num){
                if (num <= 0) {
                    System.out.println("All done!");
                    return;
                }
                System.out.println(num);
                counting(num - 1);
            }




}// END OF CLASS
