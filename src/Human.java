public class Human {

    public String firstName;
    public String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Human() {
    }




    public String getName(){
        return String.format("%s %s", firstName, lastName);
    }
    public static void main(String[] args) {
        Human firstHuman = new Human();
        firstHuman.firstName = "sandra";
        firstHuman.lastName = "linares";
        System.out.println(firstHuman.getName());
    }



}
