public class Student {

    public String Name;
    public String Cohort;
//  OVERLOADED CONSTRUCTOR  ****
    public Student(String studentName){  // custom constructor
        Name = studentName;
        Cohort = "Bravo";

    }
    public Student(String studentName, String assignedCohort){ /// custom constructor
        Name = studentName;
        Cohort = assignedCohort;
    }

    public String getInfo(){
        return String.format ("name: %s, cohort: %s", Name, Cohort);
    }

    public static void main(String[] args) {
        Student s1 = new Student("Student A");
        System.out.println(s1.getInfo());

        Student s2 = new Student("Student B", "Voyagers");
        System.out.println(s2.getInfo());

    }

}
