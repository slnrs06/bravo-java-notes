package Inherit1;

import java.util.ArrayList;
import java.util.List;

public class Main {

//    List <String> list = new ArrayList<>(); /// polymorphism

    public static void main(String[] args) {
        // declaration of the object variable a1 of the animal class

        // EXAMPLE OF POLYMORPHISM
        Animal a1;

        // object creating of the animal class
        a1 = new Animal();
        a1.displayInfo();
        // object creation of the dog class
        a1 = new Dog();
        a1.displayInfo();
        //  object creation of the cat class

        a1 = new Cat();
        a1.displayInfo();

    }



}
