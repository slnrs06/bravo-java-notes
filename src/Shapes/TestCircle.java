package Shapes;

import Shapes.Circle;


public class TestCircle {
    public static void main(String[] args) {
        // Test Constructors and toString()
//        Circle c1 = new Circle(radius);
//        System.out.println(c1);  // Circle's toString()
//        Circle c2 = new Circle(1, 2, 3.3);
//        // Test distance()
//        System.out.printf("distance is: %.2f%n", c1.distance(c2));
//        System.out.printf("distance is: %.2f%n", c2.distance(c1));
//    }
//}
    }
}


//
//public class TestCircle {
//    //This Circle class does not have a main() method. Hence, it cannot be run directly. This Circle class is a "building block" and is meant to be used in another program.
//    // Let us write a test program called TestCircle (in another source file called TestCircle.java) which uses the Circle class, as follows*/
//
//    public static void main(String[] args) {
//        // Declare an instance of Circle class called c1.
//        // Construct the instance c1 by invoking the "default" constructor
//        // which sets its radius and color to their default value.
//
//
//        Circle c1 = new Circle();
//        // Invoke public methods on instance c1, via dot operator.
//        //The circle has radius of 1.0 and area of 3.141592653589793
//        System.out.println("The circle has a radius of " + c1.getRadius() + " and an area of " + c1.getArea() + " and is colored " + c1.getColor());
//
//        // Declare an instance of class circle called c2.
//        // Construct the instance c2 by invoking the second constructor
//        // with the given radius and default color.
//        // Invoke public methods on instance c2, via dot operator.
//        //The circle has radius of 2.0 and area of 12.566370614359172
////        Circle c2 = new Circle(2.0, "blue");
////
////        System.out.println("The circle has a radius of " + c2.getRadius() + " and an area of " + c2.getArea() + " and is colored " + c2.getColor());
////
////    }
//
//
//        //The circle has radius of 2.0 and area of 12.566370614359172
//        /** Now, run the TestCircle and study the results.*/
//    }
//}
////public class TestCircle {
//
////    public static void main(String[] args) {
////
////        Circle c1 = new Circle();
////        System.out.println("The circle has radius of " + c1.getRadius() + " and area of " + c1.getArea() + " and is colored " + c1.getColor());
////
////        Circle c2 = new Circle(2.0);  // /custom constructor - enter color after r: - blue
////        System.out.println("The circle has radius of " + c2.getRadius() + " and are of " + c2.getArea() + "and is colored " + c2.getColor());
////    }
//
////}// end of class
