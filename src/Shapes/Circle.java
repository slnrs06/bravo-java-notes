package Shapes;

public class Circle {

    private double radius;
    private String color;

    public Circle (){
        radius = 1.0;
        color = "red";
    }

    public Circle(double r){        //select generate then constructor
//        this.radius = radius; // custom constructor
//        this.color = "red";
        radius = r;       //default constructor
        color = "blue";

    }

//    public String getColor() {
//        return color;
//    }
// GETTERS AND SETTERS
    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return radius*radius*Math.PI;
    }


    public String getColor() {
        return color;
    }



    public static void main(String[] args) {

    }// end of main method


}//end of class
//
//public class Circle {
//
//    //  Two private instance variables: radius (of the type double) and color (of the type String), with default value of 1.0 and "red", respectively.
//    private double radius;
//    private String color;
//
//    //  Two overloaded constructors - a default constructor with no argument, and a constructor which takes a double argument for radius.
//
//    // Constructs a Circle instance with the given radius and default color */
//    public Circle(){
//
//        radius = 1.0;
//
//        color = "red";
//    }
//
//
//    // Constructs a Circle instance with default value for radius and color */
//    public Circle(double radius, String color) {
//        this.radius = radius;
//        this.color = color;
//    }
//
//
//    //  Two public methods: getRadius() and getArea(), which return the radius and area of this instance, respectively.
//    // Returns the radius */
//    public double getRadius(){
//
//        return radius;
//    }
//
//    //Returns the area of this Circle instance */
//
//    public double getArea(){
//        return radius*radius*Math.PI;
//    }
//
//    public String getColor(){
//
//        return color;
//
//    }
//
//
//
//
//}
//
