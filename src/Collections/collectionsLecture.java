package Collections;

import java.lang.reflect.Array;
import java.util.ArrayList;

class student {
   int roll;
   String name;
}

public class collectionsLecture {

    // COLLECTIONS AND THERE ARE TWO MAIN TYPES OF COLLECTIONS USED IN JAVA THESE ARE;
//    ArrayList
//    HashMap
//  is a data structure that can be used to group or collect, objects
/*
ARRAYLIST
REPRESENTS an array that can be changed in size.  all the elements in an array must be objects and same data types.

.size() returns the number of elements in the array

.add() adds an elements to the collection at the specified index

.get() returns the element specified at the index

.index() returns the first found index of the given item. if give item not found return -1

.contain() checks to see if the arraylist contains the given element

.lastIndexOf() find the las index of the given element

.isEmpty checks to see if the list is empty.

.remove() removes the first occurrence of an item or an item at given index.
 */




   public static void main(String[] args) {
      //  EXAMPLES
      ArrayList<String> list1 = new ArrayList<String>(); /// declared an array list and defined (new) given data
      list1.add("jane");
      list1.add("jim");
      list1.add("jill");
      list1.add("jack");
      list1.remove("jim");
      list1.add("Jeb");
      System.out.println("list of list1 is:" + list1);

//      Student s1 = new Student();
//      s1.roll =  32923;
//      s1.name =  "jill";
//      ArrayList list2  = new ArrayList();





      ArrayList list2 = new ArrayList();  // not a String

      list2.add("jim");
      list2.add(10);
      System.out.println("list of list is : " + list2);

      ArrayList<Integer> numbers1 = new ArrayList<>();
      numbers1.add(1); // adding to the ArrayList element
      numbers1.add(2);
      numbers1.add(3);
      numbers1.add(4);
      numbers1.add(5);
      numbers1.add(6);

      numbers1.add(15);
//      numbers1.addAll(list2);
      System.out.println(numbers1);

      System.out.println(numbers1.get(2));  /// getting element at index of 2

      System.out.println(numbers1.set(1,8));
      System.out.println(numbers1);

      System.out.println(numbers1.remove(0)); // removing from first index
      System.out.println(numbers1);

//      Collections.sort(numbers1);

//      numbers1.get(2);

   }

//   ArrayList<String> roasts = new ArrayList<>();
//        roasts.add("medium");
//        roasts.add("dark");
//        roasts.add("light");
//        roasts.add("raw");
//        roasts.add("wellDone");
//        System.out.println(roasts);//print out arrayList
//        System.out.println(roasts.contains("bold"));//checking to see if ArrayList contains given value
//        System.out.println(roasts.lastIndexOf("light"));//return the index of given value
//        System.out.println(roasts.isEmpty());//checking if ArrayList is empty
//        roasts.clear();
//        System.out.println(roasts);
//
//
//
//}




}
