package Collections;
// Set is a child interface of collection
// If we want to represent a group of individual objects single entity where duplicates are not allowed and insertion order is not preserved we would want to go for set.

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

/** A java set does make any promises about the order elements kept internally
 * Properties of sent
 * Contains only unique elements (not common and no duplicates  (no two elements are the same)
 * Order of elements in a set is implementation dependent
 * Hashset elements are order on hash code of elements
 * This interface models the mathematical set abstractions and is used to represent set, (ex. deck of cards is a set)
 *
 * The Java collection platform contains three general Set implementations: HashSet, TreeSet, and LinkedHashSet You can use iterator of foreach loop to traverse the elements of a set.
 * */

public class Set {
    public static void main(String[] args) {


    /** Hash Set class internally uses HashMap to store the objects. The elements you enter into HashSet will be stored as Keys of HashMap and their values  will be constant.
     * HashSet does not allow duplicates, if you inset a duplicate, the older element will be overwritten
     */

    //  Creating HashSet object
    HashSet<String> set = new HashSet<String>();
    // Here we have created a HashSet called set

    HashSet<Integer> numbers = new HashSet<>();

    numbers.add(1);
    numbers.add(2);
    numbers.add(3);
//        System.out.println("numbers HashSet " + numbers);

         // calling iterator method
        Iterator<Integer> iterate = numbers.iterator();
        System.out.println("HashSet using iterator: ");
        // accessing elements below
        while (iterate.hasNext()){
            System.out.println(iterate.next());

            // using remove() metod
            boolean value1 = numbers.remove(2);
            System.out.println("is 2 removed? " + value1);
            System.out.println(numbers);

            // EXAMPLE USING TREESET
            TreeSet<Integer> trees = new TreeSet<Integer>();
            // adding elements to the treeSet
            trees.add(20);
            trees.add(30);
            trees.add(40);
            trees.add(50);
            System.out.println("your TreeSet is: " + trees);

            //Using the higher method-returns the next lowest elements among the elements greater than the specified element.
            System.out.println("using higher: " + trees.higher(40));

            // using the lower - returns the next greatest element among those elements that are less than the specified element.
            System.out.println("using lower: " + trees.lower(60));

            // Using the ceiling - return the lowest element among those that are greater than the specified element. if the element passed exists in a treeSet, it returns the element passed as an argument
            System.out.println("using ceiling " + trees.ceiling(10)); //returned 20

            // using floor - returns the greatest element among those elements that are less than the specified element. If the element passed exists in the tree set, it returns the element passed as an argument
            System.out.println("using floor " + trees.floor(20));


            /** LinkedHashSet - is an ordered version of the HashSet that maintains a doubly-linked list across all elements. Use this class instead of HashSet when you care about iteration order.  When you iterate through a HashSet the order is unpredictable, while a LinkHashSet lets you iterate through the elements in the order in which they were inserted.
             * linkedHashSet Contructors
             *
             * */
            new LinkedHashSet();  // default constructor

        //    new LinkedHashSet(Collection); // creates a LinkedHashSet from Collection c
        //    new LinkedHashSet(int capacity); // Initial
         //   capacity as a parameter

            LinkedHashSet<String> linkedSet = new
                    LinkedHashSet<String>();
            // adding element to linkedSet
            linkedSet.add("BMW");
            linkedSet.add("Honda");
            linkedSet.add("Audi");
            linkedSet.add("Volkswagen");
            linkedSet.add("BMW");  // duplicate not allowed inside of Set
            System.out.println(linkedSet);
            System.out.println("Size of linkedSet " + linkedSet.size());
            System.out.println("Removing BMW from linkedSet " + linkedSet.size());
            linkedSet.remove("BMW");
            System.out.println(linkedSet);




        }



    } // end of main method
}// end of class
