package Collections;

import java.util.ArrayList;

public class Clone {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();

        // adding elements to the arraylist
        al.add("Apple");
        al.add("orange");
        al.add("mango");
        al.add("grapes");
        System.out.println("Arraylist is " + al);

        ArrayList<String> al2 = (ArrayList<String>) al.clone();
        System.out.println("shallow copy of arraylist " + al2);

//  add and remove on original arraylist

        al.add("fig");
        al.remove("grapes");
        System.out.println(al);

// display array list after ad and remove
        System.out.println("Cloned arraylist: " + al2);
        System.out.println("original arraylist: " + al);






    }

}
