package Collections;

import java.util.ArrayList;
import java.util.jar.JarOutputStream;

// in this example we will be looking at the .set
public class setExample {
    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        System.out.println("arraylist before update:" + arrayList);

        // updating the first element
        arrayList.set(0, 11);
        System.out.println(arrayList);

        arrayList.set(1, 22);
        System.out.println(arrayList);


    }


}
