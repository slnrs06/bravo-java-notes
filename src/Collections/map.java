package Collections;

import java.security.Key;
import java.util.HashMap;
import java.util.Iterator;

/** The Map interface of the Java collections framework provides the functionality of the map data structure
 * In Java, elements of Map are stored in key/value pairs.
 * Keys are unique values associated with individual values
 * A map can not contain duplicate keys.  And each key is associated with single value.
 *
 * Note: the Map interface contains 3 different sets
 *
 * the set of keys
 * the set of values
 * the set of key/value associations individually
 * Since Map is in interface we can not create object from it.
 *
 * In order to use functionalities of the Map interface we can use these classes:
 *
 * HashMap
 * EnumMap
 * LinkedHashMap
 * WeakHashMap
 * TreeMap
 * The Map interface is also extended by these subInterfaces
 * sortedmap
 * NavigableMap
 * ConcurrentMap
 *
 * Common methods
 * .putIfAbsent - puts only a key-value pair if absent
 * .remove - removes key/value
 * .replace - replaces value at given key
 * .clear - empty the map
 * .isEmpty - check if map is empty.
 *
 * */


public class map {

    public static void main(String[] args) {

        // creating a hashMap within map each one contains
        // a capacity && a load factor

        HashMap<String, Integer> map1 = new HashMap<>();

        // example 2 Creating HashMap with 30 as initial capacity
        HashMap<String, Integer> map2 = new HashMap<String, Integer>(30, 1);

        HashMap<Integer, String> map = new HashMap<>();
        map.put(21, "Twenty One");
        map.put(32, "Thirty One");

//        Iterator<Integer>KeySetIterator = map.keySet().iterator();
//        while(KeySetIterator.hasNext()){
//            Integer key = KeySetIterator.next();
//
////            System.out.println("Key: " + key + "Value" + map.get(key));
////
////            System.out.println("does the HashMap contain 21 as a value: " + map.containsValue(21));
////
//        }

// Defining HashMap
        HashMap<String, String> usernames = new HashMap<>();

        //adding using the put method
        usernames.put("John", "Doe");
        usernames.put("Jane", "Doe");
        usernames.put("Jim", "Jameson");
        usernames.put("Johnny", "Cash");
        System.out.println(usernames);

        // obtaining values in HashMap by using key
        usernames.get("John");
        System.out.println(usernames);
        System.out.println(usernames.get("John"));

        // check if key values are present
        System.out.println(usernames.containsKey("Johnny"));




    }



}
