package Collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class CollectionHashMapExercise {
    public static void main(String[] args) {

//       Create a program that will append a specified element to the end of a hash map.
        HashMap<String, String> map = new LinkedHashMap<>();
        map.put("Black", "Dog");
        map.put("White", "Cat");
        map.put("Blue", "Bird");

        System.out.println(map);
        map.put("Orange", "Fish");




//         Create a program that will iterate through all the elements in a hash map
//
//        for(String i : map.keySet()){
//            System.out.println("Key : " + i + "Value" + map.get(key));
//        }

//        Iterator<Integer>KeySetIterator = map.keySet().iterator();
//       while(KeySetIterator.hasNext()) {
//           Integer key = KeySetIterator.next();
//           System.out.println("Key: " + key + "Value" + map.get(key));
//       }


//        Create a program to test if a hash map is empty or not.
            boolean result = map.isEmpty();
        System.out.println(" is HashMap empty" + map.isEmpty());
            map.clear();
            result = map.isEmpty();
        System.out.println("is my HashMap empty now ?" + true);


//        Create a program to get the value of a specified key in a map
//        String val=(String)map.get(blue);
//        // check the value
//        System.out.println("Value for key blue is: " + val);

        System.out.println(map.get("Dog"));


//        Create a program to test if a map contains a mapping for the specified key.
            HashMap<String, Integer> people = new HashMap<>();
            people.put("Jim", 20);
            people.put("John", 15);
            people.put("Jill", 10);
            people.put("Jane", 13);
            people.put("Jack", 19);
            if (people.containsKey("John")){
                System.out.println("John is here as a key");
            } else{
                System.out.println("no johnny B good here!");
            }


//        Create a program to test if a map contains a mapping for the specified value.
            if(people.containsValue(20)){
                System.out.println("You have a value of 20");
            }else{
                System.out.println("no value of such here....");
            }



//         Create a program to get a set view of the keys in a map

//
//                Set keySet = people.keySet();
//        System.out.println("Key set value are " + keySet);



//        Create a program to get a collection view of the VALUES contained in a map.

        System.out.println("Collection view is: " + people.values());



    }



    }


