package Collections;

import java.lang.reflect.Array;
import java.util.*;

/** List interface an ordered or sequential collection of object
 * Has some methods which can be used to stone manipulate the ordered collection of object
 * The classes which implement the list interface are called as lists.
 * These class are:
 * Arraylist
 * Vector
 * LinkedList
 * We can also use the list interface since Arraylist is implemented from list
 * In list you have control over the location on where an element is inserted or removed.
 * All 15 methods of collections util library are available in addition to
 * :
 * E get (int index) - returns element at specified index
 * e set (int index, E element) - Replaces an element at the specified position with the passed element.
 * void add(int index, E element ) - Inserts passed element at a specified index
 * E remove (int index) - this removes an element at specified index.
 * Int index (object o) - It returns an index of first occurrence of passed objects
 * int lastIndexOf(Object o) - returns an index of last occurrence of passed object.
 * ListIterator<E>listIterator() - It returns a list iterator over the elements of this list.
 * ListIterator<E>listIterator(int index) - it returns a list iterator over the elements of this list starting at the specified index.
 * List<E>subList(int fromIndex, int toIndex)- returns sub list of this list starting from index to index.
 * Examples
 * * Arraylist
 *  * Vector
 *  * LinkedList
 *
 *
 *
 */
public class List {
    public static void main(String[] args) {


// syntax for creating instance of an ArrayList
    ArrayList a = new ArrayList(); // example 1 (most commonly used)

    // linkedList example 2
    LinkedList b = new LinkedList();

    // Vector example 3
    Vector c = new Vector();

    // Stack list example 4
    Stack d = new Stack();

    // Here is how we can create an ArrayList in Java
    // Here, Type indicates the type of an array list

    // 1)   ArrayList<Type> arrayList = new ArrayList<>();
//    ArrayList <Integer> numbers = new ArrayList<>();

    // Initialize an ArrayList Using asList()
//    new ArrayList<>(Arrays.asList(("Cat", "Dog", "Cow"));

    // We can use ArrayList using the list interface
//    List<String> list = new Arraylist();

    // example creating string type of arrayList

        ArrayList<String> words = new ArrayList<>();
        // calling on collections methods from java.util
        words.add("Dog");
        words.add("Cat");
        words.add("Cow");
        System.out.println("ArrayList:" + words);
        // We can also add using indexes

        words.add(3, "Mouse");
        words.add(4, "Sheep");
        words.add(5, "Zebra");
        System.out.println("ArrayList adding with indexing:" + words);


// Create an Integer arrayList, integers is the correspondence wrapper class on int type





        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);
        System.out.println("ArrayList as an integer data type is " + nums);


        // In this example we will use the asList method to create ArrayList

//        ArrayList<String> groceries = new ArrayList<String>(Arrays
//        // then use the asList method to convert array into arrayList)
//        asList("Milk", "Chips", "Bread"));
//        System.out.println("ArrayList " + groceries);
//
//        // Access elements of the arrayList
//        String element = groceries.get(1);



        // In the following example we are using add(), addFirst(), and addLast() methods to add elements to the desired locations in the LinkedList
        LinkedList<String> list = new LinkedList<String>();

        //adding elements to the LinkedList
        list.add("Steve");
        list.add("Carl");
        list.add("Tim");
        System.out.println(list);

        // adding an element to the first position
        list.addFirst("Nyomi");
        System.out.println(list);

        // adding an element to the last position
        list.addLast("Jim");
        System.out.println(list);

        // adding an element to third position using index
        list.add(2, "Glenn");
        System.out.println(list);

        // using iterator interface
        Iterator<String> iterator = list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }











    } // end of main method


    // Linked list are linked together using pointers, each element of the linked list references to the next element of the linked list.

    // Each element of the linked list is called a Node. Each Node of the linked list contains two items.
    // 1) Content of the element
    // 2) Pointer/Address/Reference to the next Node in the list.
    // Insert and Delete operations in the linked list are not performance wise expensive because adding and deleting an element from the linked list doesnt require the element to shift.

    // In the following example we are using add(), addFirst(), and addLast() methods to add elements to the desired locations in the LinkedList
//    public void linkedList(){
//
//
//    }


//    public static class stackExample{
//        public ArrayList stack = new ArrayList();
//
//        public stackExample(ArrayList stack){
//            this.stack = stack;
//        }
//        public void push(){
//            // adding object to stack
//            stack.add(obj);
//
//        }
//        public Object pop(){
//            //Return and remove the top time from the stack
//            // throws an EmptyStackException
//            // if there are no elements in the stack
//            if(stack.isEmpty())  // conditional if stack is empty
//                throw new EmptyStackException();  // exception error
//            return stack.remove(stack.size()-1); // calling stack size and remove from index at -1
//        }
//
//        public boolean isEmpty(){
//            // test whether the stack is empty
//            return stack.isEmpty();
//        }// end of class stack
//
//
//    }






}// end of class
