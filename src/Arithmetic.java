public class Arithmetic {
    // static property
    public static double pi = 3.14159;


    // static method
    public static int add(int x, int y){
        return x + y;

    }

    // static method multiply
    public static int multiply(int x, int y){
        return x * y;
    }


}// end of class
