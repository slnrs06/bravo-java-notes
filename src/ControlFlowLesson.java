import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
//        COMPARISON OPERATORS
//        System.out.println( 5 != 2); //  true

//        System.out.println( 5 >= 5);  //true

//       LOGICAL OPERATORS ||, &&
//        System.out.println( 5 == 6 && 2>1 && 3!=3); // false
//        System.out.println( 5 != 6 && 2>1 && 3==3); // true
//        System.out.println(5 == 5 || 3 !=3); // true

//      IF STATEMENTS
        /*
        SYNTAX
        if (conditional 1 is met) {
            do task 1
            } else if{ conditional 2 is met) {
            do task 2
            }
            else{
            do task 3
            }
         */
//        int score = 34;
//        if (score >= 50) {
//            System.out.println("New High Score!");
//        }else if {
//            System.out.println("Try again...");
//        }

//          STRING COMPARISON
//            Scanner sc = new Scanner(System.in);
//        System.out.print("Would you like to continue?[y/N]");
//        String userInput = sc.next();

//        DONT DO THIS
//            boolean confirm = userInput == "y";

//        INSTEAD DO THIS
//            boolean confirm = userInput.equals("y");
//            if (confirm == true) {
//                System.out.println("Welcome to the Jungle");
//            }else if (userInput.equals("n")) {
//                System.out.println("Welcome to the Jungle..you enter a lowercase n");
//            }else {
//                System.out.println("Bye bye bye");
//            }

//              SWITCH STATEMENT
        /*
        SYNTAX:
        switch (variable used for "switching") {
            case firstCase: do take A;
                            break;
            case secondCase: do task B;
                            break
             default:       do task C:
                            break;
         */

//
//          WHILE LOOP  ------
//         SYNTAX:
        /*
            while (conditional) {
            //  loop
            }

         */
//            int i = 0;
//            while (i <= 10) {
//                System.out.println(" i is " + i);
//                i++;
//            }

//         DO WHILE LOOP  -------
        /*
            do{
            // statements
            } while (condition is true)
            }
         */
//            do {
//
//                System.out.println("You will see this!");
//            } while (false);

        //    FOR LOOP   ********
//            for  (var i = 0; i <= 10; i++) {
//                System.out.println(i);
//            }

//          ESCAPE SEQUENCES - DEALING WITH STRINGS
        System.out.println("Hello \nWorld");  // two separate lines
        System.out.println("Hello \tWorld");  // tabs over  hello  world




    }

} // END OF CLASS
