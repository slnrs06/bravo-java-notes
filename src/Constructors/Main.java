package Constructors;
public class Main {
    public static void main(String[] args) {
        Shirt s = new Shirt();//shirt is the default constructor
        s.setColor("White");
        s.setSize('M');
        System.out.println(s.color);
        System.out.println(s.size);
        s.takeOff();
        s.putOn();

    }
}
