import java.util.Scanner;

public class ControlFlowExercise {
    public static void main(String[] args) {

//        Create an integer variable i with a value of 9.
//        Create a while loop that runs so long as i is less than or equal to 23
//        Each loop iteration, output the current value of i, then increment i by one
//
//        int i = 9;
//        while (i <= 23) {
//            System.out.print(" "+ i + " ");
//            i++;
//        }


//        Do While
//        Create a do-while loop that will count by 2's starting with 0 and ending at 100.
//               int i = 0;
////        do {
////            System.out.println(i);
////            i+=2;
////        } while (i <= 100);


//        Alter your loop to count backwards by 5's from 100 to -10.

//       int i = 100;
//       do {
//           System.out.println(i);
//           i-=5;
//       }while (i >= -10);



//        Create a do-while loop that starts at 2, and displays
//        the number squared on each line while the number is
//        less than 1,000,000. Output should equal:
//        2
//        4
//        16
//        256
//        65536
//
//      adrian's code
//        long number = 2;
////        int num = 2;
//        do {
//            System.out.println(number);
//            number *= number;
//        } while (number <= 1000000);

        //  stephen code  ----  square number
//
//          er = 2;
//            do {
//                System.out.println(number);
//                number = (int)Math.pow(number, 2);
//
//            }while (number <= 1000000);

//          FIZZBUZZ
//        Write a program that prints the numbers from 1 to 100.
//        For multiples of three print "Fizz" instead of the number
//        For the multiples of five print "Buzz".
//        For numbers which are multiples of both three and five print "FizzBuzz".
//        Display a table of powers.
//        Prompt the user to enter an integer.
//        Display a table of squares and cubes from 1 to the value entered.
//        Ask if the user wants to continue.
//        Assume that the user will enter valid data.
//        Only continue if the user agrees to.
//
//        stephen code
        //        Write a program that prints the numbers from 1 to 100.
//            for(int x = 0; x <= 100; x++) {
//                if(x % 15 == 0) {
//                    System.out.println("FizzBuzz");
//                } else if (x % 5 == 0) {
//                    System.out.println("Buzz");
//                }else if (x % 3 == 0){
//                    System.out.println("Fizz");
//                }else{
//                    System.out.println(x);
//                }
//            }

//        Display a table of powers.

//        System.out.println("what number would you like to go up to");
//            Scanner input = new Scanner(System.in);
//            int num = input.nextInt();
//
//        System.out.println("here is your table");
//        System.out.println("NUMBER | SQUARE | CUBE");
//        System.out.println("- - - -| - - - -| - - - -");
//        for (int i = 1; i <= num; i++) {
//            int num1 = i;
//            int num2 = (int)Math.pow(i, 2);
//            int num3 = (int)Math.pow(i, 3);
////            System.out.println("\t" + num1 + " | \t" + num2 + "  |\t" + num3);
//            System.out.printf("%-6d | %-6d | %-6d%n", num1, num2, num3);
//        }


//              GRADING SYSTEM   Convert given number grades into letter grades.
            Scanner scanner = new Scanner(System.in);
            String input;
            do {
                System.out.println("Enter a grade number: ");
                int grade = scanner.nextInt();
                if (grade >= 90 && grade <= 100) {
                    System.out.println("you got a A");
                } else if (grade >= 80 && grade <= 90) {
                    System.out.println("you got a B");
                } else if (grade >= 70 && grade <= 80) {
                    System.out.println("you got a C");
                } else if (grade >= 60 && grade <= 70) {
                    System.out.println("you got a D");
                } else if (grade >= 0 && grade <= 60) {
                    System.out.println("you got a F");
                } else {
                    System.out.println("you did not enter a grade value (0 - 100)");
                }
                System.out.println("would you like to continue?");
                input = scanner.nextLine();
            } while (input.equalsIgnoreCase("yes"));
        System.out.println("have a nice day!");



    }
}// END OF CLASS
