public class Person {
//   EXAMPLE 1 FROM THE TOP COMMENT OUT PREVIOUS CODE TO WORK
// creating instance variables
    public static long worldPopulation = 75000000009040385L; // class property
    public String firstName;
    public String lastName;

//instance method
//    public String sayHello(){
//        return String.format("Hello from %s %s", firstName, lastName);
//    }

// main method
    public static void main(String[] args) {
        Person theBestDrummerAlive = new Person();
            theBestDrummerAlive.firstName = "Neil Hart";
            Person.worldPopulation += 1; // accessing static property
        System.out.println(Person.worldPopulation);


//        Person rick = new Person();
//        rick.firstName = "Ricky";
//        rick.lastName = "Sanchez";
//        System.out.println(rick.sayHello());

//        System.out.println(Math.PI);



    }// end of main method

} // END OF CLASS
