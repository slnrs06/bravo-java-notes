public class Spurs {
    // creating instance variables
    public String firstName;
    public String lastName;
    public int jerseyNumber;


    // instance methods
    public String sayHello(){
        return String.format("Hello from %s %s %d", firstName, lastName, jerseyNumber);
    }

    public static void main(String[] args) {    //creating spurs objects and assigning variables
        Spurs player1 = new Spurs();

        player1.firstName="Manu";
        player1.lastName="Ginobili";
        player1.jerseyNumber=20;
        System.out.println(player1.sayHello());

        Spurs player2 = new Spurs();

        player2.firstName="Timmy";
        player2.lastName="Duncan";
        player2.jerseyNumber=21;
        System.out.println(player2.sayHello());

    }// end of main method




}
