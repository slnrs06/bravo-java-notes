public class Car {
    // instance variables objects  JAVA II
    public String name;
    public String maker;
    public String model;
    public int year;

    // Instance method
    public String startTheCar(){
        return String.format("Starting up as %s %s %s %d", name, maker, model,year);

    }

    // the car / CLASS constructor //  special method object is created/must be name of class name
        public Car(){
            System.out.println("A car is being made!");
        }

    public static void main(String[] args) {
        // CREATE an instance of our Car Class

        Car firstCar = new Car();
        firstCar.name = "Betty";
        firstCar.maker = "Chevy";
        firstCar.model = "Corvette";
        firstCar.year = 1975;
        System.out.println(firstCar.startTheCar());


        Car car2 = new Car();
        car2.name = "Speedy";
        car2.maker = "Ford";
        car2.model = "Mustang";
        car2.year = 1965;
        System.out.println(car2.startTheCar());








    } // end of main method




}//  end of class
