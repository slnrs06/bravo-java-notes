package Movies;

import java.util.Arrays;
import java.util.Scanner;

public class Movie {
    // FIELDS
    private String name;
    private String category;

    // SCANNER
    private static Scanner scanner = new Scanner(System.in);


    // CONSTRUCTOR
    public Movie(String name, String category) {
        this.name = name;
        this.category = category;
    }

    // GETTERS / SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static void movieMenu() {
        System.out.println("Welcome to BlockBuster!");
        System.out.println("0 - exit");
        System.out.println("1 - view all movies");
        System.out.println("2 - view animated movies");
        System.out.println("3 - view drama movies");
        System.out.println("4 - view horror movies");
        System.out.println("5 - view scifi movies");
        System.out.println("6 - view musical movies");
        System.out.println("7 - view comedy movies");
        System.out.println("8 - add a movie");

    }

    // FILTERING MOVIES BASED ON CATEGORY
    public static Movie[] filterMovies(Movie[] movies, String category) {
        Movie[] filteredMovies = new Movie[0];
        // for loop
        for(Movie movie : movies) {
            // if statement
            if(movie.getCategory().equals(category)) {
                filteredMovies = addToCurrentMovies(filteredMovies, movie);
            }
        }
        return filteredMovies;
    }

    // ADD TO CURRENT MOVIES
    public static Movie[] addToCurrentMovies(Movie[] movies, Movie movie) {
        Movie[] currentMovies = Arrays.copyOf(movies, movies.length + 1);
        currentMovies[currentMovies.length - 1] = movie;
        return currentMovies;
    }

    // NEW MOVIE
    public static Movie newMovie(){
        System.out.println("Enter movie title: ");
        scanner.nextLine();
        String title = scanner.nextLine();
        System.out.println("Enter movie category: ");
        String category = scanner.nextLine();
        return new Movie(title, category); // creating a new Movie object
    }

    // PRINT MOVIES
    public static void printMovies(Movie[] movies) {
        for (Movie m : movies) {
            System.out.println("\nTitle: " + m.getName() + "\nCategory: " + m.getCategory());
            System.out.println("\n=======================================");
        }
    }

    // PRINT MOVIE RESULT (SELECTION)
    public static void printMovieResults(int userSelection){
        Movie[] filteredMovies;
        Movie[] movies = Arrays.copyOf(MoviesArray.findAll(), MoviesArray.findAll().length);

        switch (userSelection){
            case 0:
                System.out.println("Exiting");
                break;
            case 1:
                printMovies(movies);
                break;
            case 2:
                filteredMovies = filterMovies(movies, "animated");
                printMovies(filteredMovies);
                break;
            case 3:
                filteredMovies = filterMovies(movies, "drama");
                printMovies(filteredMovies);
                break;
            case 4:
                filteredMovies = filterMovies(movies, "horror");
                printMovies(filteredMovies);
                break;
            case 5:
                filteredMovies = filterMovies(movies, "scifi");
                printMovies(filteredMovies);
                break;
            case 6:
                filteredMovies = filterMovies(movies, "musical");
                printMovies(filteredMovies);
                break;
            // Calling addToCurrentMovies and passing in the newMovie() as a parameter
            case 7:
                filteredMovies = filterMovies(movies, "comedy");
                printMovies(filteredMovies);
                break;
            case 8:
                movies = addToCurrentMovies(movies, newMovie());
                printMovies(movies);
                break;
            default:
                System.out.println("Not a valid selection");
                break;
        }
    }

    // START THE MOVIE APPLICATION
    public static void startMovieApplication() {
        int userSelection;
        // do while loop
        do {
            movieMenu();
            userSelection = scanner.nextInt();
            printMovieResults(userSelection);
        } while (userSelection != 0);
    }
}



//    public Movie(String name, String category) {
//        this.name = name;
//        this.category = category;
//    }
//
//    public String[] getMovie(String [] name){
//        return name;
//    }
//    public void setMovie(){
//        name = name;
//    }
//    public String[] getCategory( String [] category){
//        return category;
//    }
//    public void setCategory(){
//        category = category;
//    }





//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getCategory() {
//        return category;
//    }
//
//    public void setCategory(String category) {
//        this.category = category;
//    }


