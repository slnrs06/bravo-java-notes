public class StringsLesson {
    public static void main(String[] args) {

        //  STRINGS  -----------
//        String message = "Hello World";
//        System.out.println(message);
////
//        String anotherMessage;
//        anotherMessage = "Another message assigned!";
//        System.out.println(anotherMessage);

//        CONCATENATION  --------------
//        String myName = "Hello World, " + "my name is Stephen";
//        System.out.println(myName);
//        System.out.println(message + " " + anotherMessage +  " " + myName);

//        String message = "Hello World";
//          if(message=hello world) // do not use
//        if (message.equals("Hello World")) {
//            System.out.println("Message is Correct");
//        }

//          STRING COMPARISON METHODS  ----------
//        .equals(), . equalsIgnoreCase(), .startsWith(), .endsWith()

        String input = "Bravo Rocks!";
//        input.equals("Bravo Rocks");
        System.out.println(input.equals("Bravo Rocks!"));// true
        System.out.println(input.equals("bravo rocks!"));  // false

        System.out.println(input.equalsIgnoreCase("BRAVO ROCKS!"));  //true
        System.out.println(input.equalsIgnoreCase("BRAVO ROCK!")); // false

        System.out.println(input.startsWith("Bravo")); // true
        System.out.println(input.startsWith("brave")); // false

        System.out.println(input.endsWith("Rocks!")); // true
        System.out.println(input.endsWith("rocks!")); // false


    }

} // end of class
