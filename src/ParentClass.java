public class ParentClass {
    //parent class constructor below


    public ParentClass() {//  from generate -constructor menu
        System.out.println("Constructor of parent class");

    }

    void display(){
        System.out.println("I am a parent method named display !");
    }
}

class JavaExample extends ParentClass{
    JavaExample(){
        System.out.println("constructor of child");

        //By default this JavaExample constructor invokes the constructor of
        // the parent class
        // You can use super() to call the constructor of the parent.
        // It should be the first statement in the child class.

        // Constructor can also call the parameterized constructor of the parent class by using super like  super(10), now this will invoke the parameterized of arg.


    }

    void display(){
        System.out.println("Child method who overrides parents method");

        super.display();  // refers to super class is parent class  calling hierarchy method/

    }


//    class SuperHero extends Person{
//        private String alterEgo;
//
//        public SuperHero(String name, String alterEgo){
//            super(name);
//
//            this.alterEgo = alterEgo;
//        }
//        public String getName();
//        return alterEgo;
//
//
//
//    }


    public static void main(String[] args) {
        JavaExample obj = new JavaExample();
        obj.display();

//        new JavaExample();

    }
}
