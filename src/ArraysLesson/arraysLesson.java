package ArraysLesson;

import java.util.Arrays;

public class arraysLesson {
    // ARRAYS
    // LIST OF DATA THAT CONTAINS ZERO OR MORE ITEMS CALLED ELEMENTS
    // ARRAYS CAN ONLY BE OF ONE DATA TYPE WITHIN A SINGLE ARRAY

    // SYNTAX FOR ARRAY

    // declaring an array of datatype double assigning variable of prices
    double[]prices;
    int[] integers;
    String[] names;
    float[] floatingNumbers;

    public static void main(String[] args) {
//       String[] names = new String[6];
//       names[0] = "Adrian";
//       names[1] = "Sandra";
//       names[2] = "MaryAnn";
//       names[3] = "Henry";
//       names[4] = "Jonathon";
//       names[5]= "Eric";
//        System.out.println(names[0]);
//
//        System.out.println(names[3]);
//
//
//        // assign values {""} within array
//        String[] avengers = {"Captain America", "Iron Man", "Hulk", "HawkEye", "Thor", "Black Widow"};
//
//        System.out.println(avengers.length);

        //multi dimensional arrays

//  ITERATING OVER ARRAYS
        int [] numbers = new int[5];
        numbers [0] = 1;
        numbers [1] = 2;
        numbers [2] = 3;
        numbers [3] = 4;
        numbers [4] = 5;

        for(int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }


        String[] languages = {"html", "css", "javascript", "angular","java"};
        for (int i = 0; i < languages.length; i++){
            System.out.println(languages[i]);
        }

            // enhanced for loop with same result of printing the array

        for (String language : languages){
            System.out.println(language);
        }


        //  example of iterating through 2d array

        int [] [] matrix ={
                {1,2,3},
                {4,5,6},
                {7,8,9}

        };

        for (int i = 0; i < matrix.length; i++){
            System.out.println(Arrays.toString(matrix[i]));
        }





    }// end of main method










}// end of class
