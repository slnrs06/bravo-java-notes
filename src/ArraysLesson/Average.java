package ArraysLesson;

import java.util.Scanner;

public class Average {
// LOOPING FOR AN ARRAY

    public static Scanner scanner = new Scanner(System.in);// CREATED SCANNER TO RECEIVE INPUT

// MAIN METHOD
    public static void main(String[] args) {

//DECLARING/PRINTING AN INT ARRAY DECLARING VARIABLE myIntArray AND ASSIGNING IT getIntegers METHOD PUTTING VALUE IN () parameters
        int[] myIntArray = getIntegers(5);
        //  PRINT ARRAY CALLED myIntArray
        printArray(myIntArray);

        //PRINTING OUT THE AVG BY CALLING getAverage METHOD TAKES IN myIntArray
        System.out.println("Average is " + getAverage(myIntArray));

    }

//  CREATING A METHOD TO geIntegers TAKES IN A NUMBER PRINTS OUT INTEGERS IN THE ARRAY
    public static int[] getIntegers(int number){
        System.out.println("Please enter " + number + " integer values ");
        int intArray[]= new int[number];  ///  DECLARING AN ARRAY CREATING ARRAY OBJECTS TAKING IN INT NUMBER WITH IN METHOD
        for (int i = 0; i<number; i++){   // LOOPING THROUGH NUMBER
            intArray[i] = scanner.nextInt();  // ASSIGNING INT ARRAY SCANNER TO THE nextInt

        }

        return intArray;  // returning the array

    }
    public static void printArray(int[] intArray){
        for (int i = 0; i<intArray.length; i++){     //  LOOPING THROUGH ARRAY
            System.out.println(intArray[i]);           // PRINTING OUT ITEMS INDEXED IN ARRAY
        }


    }

    public static double getAverage(int[] intArray){    //  METHOD TO GET AVERAGE
        int sum = 0;             /// CREATING SUM OBJECT AND ASSIGNING VALUE OF 0
        for (int i = 0; i<intArray.length; i++){      // LOOPING THROUGH intArray USING i++ TO PERFORM SUM
            sum += intArray[i];    // NOW ASSIGNING SUM OBJECT TO intArray
        }
        return (double) sum /(double) intArray.length;   // returning sum dividing it by length of array
    }



}// end of class
